Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Starjava TFCAT
Upstream-Contact: Mark Taylor <m.b.taylor@bristol.ac.uk>
Source: https://github.com/Starlink/starjava/tree/master/tfcat

Files: *
Copyright: 2005-2018 Central Laboratory of the Research Councils
 2018 Mark Taylor
 2018 Ole Streicher <olebole@debian.org> (Debian files)
License: LGPL-2.1+
Comment: The license information is stored in the root of the upstream
 git repository.
 .
 This license is hereby asserted to apply to all the original
 (non-third-party) code and associated files in the packages
 from this starjava repository, specifically:
 .
   array      diva  jaiutil   ndx         splat     ttools
   astgui     fits  jniast    pal         srb       util
   astrogrid  frog  jnihds    plastic     startask  vo
   cdf        gbin  jnikappa  registry    table     votable
   coco       hds   jpcs      rv          task      xdoc
   connect    hdx   mirage    soapserver  topcat
   datanode   help  ndtools   sog         treeview
 .
 In particular this license applies to all the java classes in
 or under the uk.ac.starlink namespace.
 .
 If LGPL licensing is not acceptable for particular usage requirements,
 it may be possible to provide the code under other licensing arrangements.

License: LGPL-2.1+
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
